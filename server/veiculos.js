const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var veiculoSchema = new Schema({
  valor: Number,
  IPVA: Number,
  marcas: [{type: mongoose.Schema.Types.ObjectId, ref: 'marcas'}]

}, {versionKey: false});

module.exports = mongoose.model('Veiculos', veiculoSchema);
