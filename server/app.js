const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');
const body_parser = require('body-parser');
const marcas_controller = require('./marcas_controller');

const app = express();

app.use(body_parser.json());
app.use(body_parser.urlencoded({
  extended: true
}));
app.use(cors());

mongoose.connect('mongodb://localhost:27017/http_app',{userNewUrlParser: true});

app.use('/marcas', marcas_controller);
//app.use('/veiculos', veiculos_controller);

app.listen(3000);
