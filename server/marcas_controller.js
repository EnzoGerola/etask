const express = require('express');
var router = express.Router();
var Marcas = require('./marcas');

router.post('/', function(req, res){
  console.log(req.body);
  let m = new Marcas({nome: req.body.nome});
  m.save((err, dep) => {
    if(err){
      res.status(500).send(err);
    }else{
      res.status(200).send(dep);
    }
  })
});

router.get('/', function(req, res){
  Marcas.find().exec((err, deps) => {
    if(err){
      res.status(500).send(err);
    }else{
      res.status(200).send(deps);
    }
  })
});

module.exports = router;
