const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var marcaSchema = new Schema({
  modelo: String,
  nome: String
}, {versionKey: false});

module.exports = mongoose.model('Marcas', marcaSchema);
