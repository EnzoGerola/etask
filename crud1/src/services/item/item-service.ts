import { Injectable } from '@angular/core';
import { Item } from '../../models/item/item.model';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()

export class ItemService{

  private itemRef = this.db.list<Item>('item');

  constructor(public db: AngularFireDatabase){}

  getItem(){
    return this.itemRef;
  }

  addItem(item: Item){
    return this.itemRef.push(item);
  }

  editItem(item: Item){
    return this.itemRef.update(item.key, item);
  }

  removeItem(item: Item){
    return this.itemRef.remove(item.key);
  }

}
