import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AdicionarItemPage } from '../adicionar-item/adicionar-item';
import { EditItemPage } from '../edit-item/edit-item';

import { ItemService } from '../../services/item/item-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  itemList$: Observable<Item[]>

  constructor(public navCtrl: NavController, private itemService: ItemService) {

    this.pushPage = AdicionarItemPage;
    this.edit_item = EditItemPage;

    this.itemList$ = this.itemService
    .getItem() //Get Item from DB
    .snapshotChanges() //Key e Valores
    .map(
      changes => {
        return changes.map(c =>({
          key: c.payload.key,
          ...c.payload.val()
        }));
      }
    )
  }

}
