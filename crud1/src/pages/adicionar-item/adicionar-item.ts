import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Item } from '../../models/item/item.model';
import { ItemService } from '../../services/item/item-service';
import { ToastService } from '../../services/toast/toast.service';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-adicionar-item',
  templateUrl: 'adicionar-item.html',
})
export class AdicionarItemPage {

  item: Item = {
    nome_prod: '',
    qtd_prod: undefined,
    email_cli: '',
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private itemService: ItemService,
    private toast: ToastService
  ) {}

  ionViewDidLoad() {
    console.log('ionView = AdicionarItemPage');
  }

  addItem(item: Item){
    this.itemService.addItem(item).then(ref => {
      this.toast.show(`${item.nome_prod} foi cadastrado!`);
      this.navCtrl.setRoot(HomePage, {key: ref.key})
    });
  }

}
