import { HomePage } from '../home/home';
import { Component } from '@angular/core';
import { Item } from '../../models/item/item.model';
import { ItemService } from '../../services/item/item-service';
import { ToastService } from '../../services/toast/toast.service';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EditItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-item',
  templateUrl: 'edit-item.html',
})
export class EditItemPage {

  item: Item;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private itemService: ItemService,
    private toast: ToastService
  ) {}

  ionViewWillLoad() {
    this.item = this.navParams.get('item');
  }

  salvarItem(item: Item){
    this.itemService.editItem(item).then(() => {
      this.toast.show(`${item.nome_prod} foi salvo!`);
      this.navCtrl.setRoot(HomePage)
    });
  }

  deletarItem(item: Item){
    this.itemService.removeItem(item).then(() => {
      this.toast.show(`${item.nome_prod} foi removido!`);
      this.navCtrl.setRoot(HomePage)
    });
  }

}
