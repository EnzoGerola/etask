import { Marcas } from './marcas';

export interface Veiculos {
  marcas: Marcas[];
  valor: Number,
  IPVA: Number,
  _id?: string;
}
