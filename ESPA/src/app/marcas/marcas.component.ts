import { Component, OnInit } from '@angular/core';
import { Marcas } from '../marcas';

@Component({
  selector: 'app-marcas',
  templateUrl: './marcas.component.html',
  styleUrls: ['./marcas.component.css']
})
export class MarcasComponent implements OnInit {

  marcaVeic: string = '';
  marcas: Marcas[] = [
    {nome: 'Tesla', _id: '1'},
    {nome: 'Hyundai', _id: '2'},
    {nome: 'Nissan', _id: '3'},
    {nome: 'Wolksvagen', _id: '4'},
    {nome: 'Auston Martin', _id: '5'}
  ];

  constructor(private marcasService: MarcasService) { }

  ngOnInit() {
    this.marcasService.get()
    .subscribe((marcs) => this.marcas = marcs);
  }

  salvar(){
    this.marcasService.add({nome: this.marcaVeic})
    .subscribe(
      (marc) => {
        console.log(marc);
      },
      (err) => console.error(err))
    )
  }

  editar(marc: Marcas){

  }

  excluir(marc: Marcas){

  }

}
