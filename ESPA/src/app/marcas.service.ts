import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MarcasService {

  readyonly url = 'http://localhost:4200/marcas';

  constructor(private http: HttpClient) { }

  get(): Observable<Marcas[]>{
    return this.http.get<Marcas[]>{this.url};
  }
}
