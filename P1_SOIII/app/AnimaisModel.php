<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnimaisModel extends Model
{
  public $table = "animais";
  protected $primaryKey = 'id_animal';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'a_tipo', 'a_peso', 'a_preco', 'a_vendido', 'a_sexo', 'a_vacin', 'a_vacinas', 'a_data_nasc', 'a_raca'
  ];
}
