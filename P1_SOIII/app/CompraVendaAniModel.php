<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompraVendaAniModel extends Model
{
  public $table = "Vendas_animais";
  protected $primaryKey = 'id_venda';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'id_animal', 'a_peso', 'a_preco', 'a_qtd', 'a_raca', 'a_tipo', 'a_gen'
  ];
}
