<?php

namespace App\Http\Controllers;

use App\AnimaisModel;
use Illuminate\Http\Request;

class AniSuinController extends Controller
{

    //VER ANIMAIS
    public function NovoAnSuin()
    {
        return view('AnViews.AnViewSuin.NovoAnSuin');
    }

    public function SuinosVacinados(AnimaisModel $Animal)
    {
        $Animal = AnimaisModel::where(['a_vacin' => '1', 'a_tipo' => 'suino'])->get();
        return view('AnViews.AnViewSuin.SuinVacinados')->with('Animal', $Animal);
    }

    public function SuinosNaoVacinados(AnimaisModel $Animal)
    {
        $Animal = AnimaisModel::where(['a_vacin' => '0', 'a_tipo' => 'suino'])->get();
        return view('AnViews.AnViewSuin.SuinNaoVacinados')->with('Animal', $Animal);
    }

    public function EstatsSuinos(AnimaisModel $Animal)
    {
        $Animal = AnimaisModel::where('a_tipo', 'suino')->get();
        $qtd_animais_suinos = AnimaisModel::where('a_tipo', 'suino')->get()->count();

        $qtd_vacinados = AnimaisModel::where(['a_vacin' => '1', 'a_tipo' => 'suino'])->get()->count();
        
        $qtd_nao_vacinados = AnimaisModel::where(['a_vacin' => '0', 'a_tipo' => 'suino'])->get()->count();

        return view('AnViews.AnViewSuin.EstatsSuinos', [
          'Animal'=> $Animal,
          'qtd_animais_suinos'=>$qtd_animais_suinos,
          'qtd_vacinados'=>$qtd_vacinados,
          'qtd_nao_vacinados'=>$qtd_nao_vacinados
        ]);
    }
}
