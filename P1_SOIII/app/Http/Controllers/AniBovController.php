<?php

namespace App\Http\Controllers;

use App\AnimaisModel;
use Illuminate\Http\Request;

class AniBovController extends Controller
{
    //VER ANIMAIS
    public function NovoAnBov()
    {
        return view('AnViews.AnViewBov.NovoAnBov');
    }

    public function BovinosVacinados(AnimaisModel $Animal)
    {
        $Animal = AnimaisModel::where(['a_vacin' => '1', 'a_tipo' => 'bovino'])->get();
        return view('AnViews.AnViewBov.BovinVacinados')->with('Animal', $Animal);
    }

    public function BovinosNaoVacinados(AnimaisModel $Animal)
    {
        $Animal = AnimaisModel::where(['a_vacin' => '0', 'a_tipo' => 'bovino'])->get();
        return view('AnViews.AnViewBov.BovinVacinados')->with('Animal', $Animal);
    }

    public function EstatsBovinos(AnimaisModel $Animal)
    {
        $Animal = AnimaisModel::where('a_tipo', 'bovino')->get();
        $qtd_animais_bovinos = AnimaisModel::where('a_tipo', 'bovino')->get()->count();

        $qtd_vacinados = AnimaisModel::where(['a_vacin' => '1', 'a_tipo' => 'bovino'])->get()->count();

        $qtd_nao_vacinados = AnimaisModel::where(['a_vacin' => '0', 'a_tipo' => 'bovino'])->get()->count();

        return view('AnViews.AnViewBov.EstatsBovinos', [
          'Animal'=> $Animal,
          'qtd_animais_bovinos'=>$qtd_animais_bovinos,
          'qtd_vacinados'=>$qtd_vacinados,
          'qtd_nao_vacinados'=>$qtd_nao_vacinados
        ]);
    }
}
