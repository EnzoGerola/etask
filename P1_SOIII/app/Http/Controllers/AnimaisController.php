<?php

namespace App\Http\Controllers;

use App\AnimaisModel;
use App\CompraVendaAniModel;
use Request;

class AnimaisController extends Controller
{

    //VER ANIMAIS
    public function IndexAni()
    {
        $animais = AnimaisModel::where('a_vendido', '0')->get();
        return view('AnViews.AnIndex')->with('animais', $animais);
    }

    public function AnEstats(AnimaisModel $Animal)
    {
        $Animal = AnimaisModel::all()->count();

        $peso_total_rebanho = AnimaisModel::whereNotNull('a_peso')->sum('a_peso');

        $qtd_vacinados_rebanho = AnimaisModel::where('a_vacin', '1')->get()->count();

        $preco_total_rebanho = AnimaisModel::whereNotNull('a_preco')->sum('a_preco');

        $qtd_bovin_femea = AnimaisModel::where(['a_tipo' => 'bovino', 'a_sexo' => 'femin'])->get()->count();
        $qtd_suin_femea = AnimaisModel::where(['a_tipo' => 'suino', 'a_sexo' => 'femin'])->get()->count();

        $qtd_bovin_macho = AnimaisModel::where(['a_tipo' => 'bovino', 'a_sexo' => 'masc'])->get()->count();
        $qtd_suin_macho = AnimaisModel::where(['a_tipo' => 'suino', 'a_sexo' => 'masc'])->get()->count();

        return view('AnViews.AnEstats')->with([
          'Animal' => $Animal,
          'peso_total_rebanho' => $peso_total_rebanho,
          'qtd_vacinados_rebanho' => $qtd_vacinados_rebanho,
          'preco_total_rebanho' => $preco_total_rebanho,
          'qtd_bovin_femea' => $qtd_bovin_femea,
          'qtd_bovin_macho' => $qtd_bovin_macho,
          'qtd_suin_femea' => $qtd_suin_femea,
          'qtd_suin_macho' => $qtd_suin_macho
        ]);
    }

    public function VerAnimais(AnimaisModel $Animal)
    {
        return view('AnViews.AnVer')->with('Animal', $Animal);
    }

    //CADASTRAR ANIMAL
    public function NovoAni()
    {
        return view('AnViews.AnSelec');
    }

    public function RegistraAni(AnimaisModel $Animal)
    {
        $this->validate(request(), [
          'a_tipo' => 'required',
          'a_peso' => 'required',
          'a_sexo' => 'required',
          'a_vacin' => 'required'
        ]);

        $animais_data = request()->all();

        $Animal->a_tipo = $animais_data['a_tipo'];

        if ($animais_data['a_vacin'] == '1') {
            $Animal->a_vacinas = $animais_data['a_vacinas'];
        } else {
            $Animal->a_vacinas = null;
        }

        $Animal->a_raca = $animais_data['a_raca'];
        $Animal->a_peso = $animais_data['a_peso'];
        $Animal->a_preco = ($animais_data['a_peso'] / 30) * 155;
        $Animal->a_vendido = 0;
        $Animal->a_sexo = $animais_data['a_sexo'];
        $Animal->a_vacin = $animais_data['a_vacin'];
        $Animal->a_data_nasc = $animais_data['a_data_nasc'];
        $Animal->save();

        session()->flash('success', 'Animal foi registrado com sucesso!');

        return redirect('/IndexAni');
    }

    //EDITAR ANIMAL
    public function EditarAni(AnimaisModel $Animal)
    {
        return view('AnViews.AnEdit')->with('Animal', $Animal);
    }

    public function AttAnimal(AnimaisModel $Animal)
    {
        $this->validate(request(), [

          'a_tipo' => 'required',
          'a_peso' => 'required',
          'a_sexo' => 'required',
          'a_vacin' => 'required'

        ]);

        $animais_data = request()->all();

        $Animal->a_raca = $animais_data['a_raca'];
        $Animal->a_tipo = $animais_data['a_tipo'];
        $Animal->a_peso = $animais_data['a_peso'];
        $Animal->a_preco = ($animais_data['a_peso'] / 30) * 155;
        $Animal->a_vendido = 0;
        $Animal->a_sexo = $animais_data['a_sexo'];
        $Animal->a_vacin = $animais_data['a_vacin'];

        if ($animais_data['a_vacin'] == '1') {
            $Animal->a_vacinas = $animais_data['a_vacinas'];
        } else {
            $Animal->a_vacinas = null;
        }

        $Animal->a_data_nasc = $animais_data['a_data_nasc'];

        $Animal->save();
        return redirect('/IndexAni');
    }
    //FIM EDITAR REGISTRO

    //COMPRA ANIMAL
    public function ComprarAnimal(AnimaisModel $Animal, CompraVendaAniModel $Compra){

      $animais_data = request()->all();
      $Compra->id_animal = $Animal->id_animal;
      $Compra->a_peso = $Animal->a_peso;
      $Compra->a_preco = $Animal->a_preco;
      $Animal->a_vendido = 1;
      $Compra->a_qtd = 1;
      $Compra->a_raca = $Animal->a_raca;
      $Compra->a_tipo = $Animal->a_tipo;
      $Compra->a_gen = $Animal->a_sexo;

      $Compra->save();
      $Animal->save();
      session()->flash('success', 'Muito bem! Você acaba de comprar este animal.');
      return redirect('/IndexAni');
    }

    public function VerCarrinho(CompraVendaAniModel $Compra){
      $Compra = CompraVendaAniModel::all();
      return view('AnViews.Carrinho')->with('Compra', $Compra);
    }

    //DELETA REGISTRO
    public function DeletaAnimal(AnimaisModel $Animal)
    {
        $Animal->delete();
        session()->flash('success', 'Registro removido com sucesso!');
        return redirect('/IndexAni');
    }
}
