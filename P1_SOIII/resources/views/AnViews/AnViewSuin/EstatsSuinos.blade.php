<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Estatísticas Suínos</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- BTS MD -->
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

        <!-- JQUERY IMPORT -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #181616;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                max-width: 100%;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: left;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                display: grid;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .card1:hover{
              cursor: pointer;
              box-shadow: 0 19px 38px rgba(255, 255, 255, 0.3), 0 15px 12px rgba(255, 252, 252, 0.22);
            }
            .card2:hover{
              cursor: pointer;
              box-shadow: 0 19px 38px rgba(255, 255, 255, 0.3), 0 15px 12px rgba(255, 252, 252, 0.22);
            }

            h1{
              text-shadow: 0 1px 0 #ccc,
               0 2px 0 #c9c9c9,
               0 3px 0 #bbb,
               0 4px 0 #b9b9b9,
               0 5px 0 #aaa,
               0 6px 1px rgba(0,0,0,.1),
               0 0 5px rgba(0,0,0,.1),
               0 1px 3px rgba(0,0,0,.3),
               0 3px 5px rgba(0,0,0,.2),
               0 5px 10px rgba(0,0,0,.25),
               0 10px 10px rgba(0,0,0,.2),
               0 20px 20px rgba(0,0,0,.15);
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
              <div class="row justify-content-center">

                  <div class="card1" style="width: 15rem; height: 25rem; background: linear-gradient(to bottom, #0aaf29 1%,#474545 100%,#060606 100%); border-radius: 15px; text-align: center;" id="primeiro_card">
                    <div class="card-body">
                      <h5 class="card-title"></h5>
                      <div class="card-body">
                        <h1 class="card-title">{{ number_format((float)100/$qtd_animais_suinos*$qtd_vacinados, 2, '.', '') }} %</h1>
                        <h2 class="card-text" style="color: white;">Suínos Vacinados</h2>
                      </div>
                    </div>
                  </div>
                  <div class="card2" style="width: 15rem; height: 25rem; background: linear-gradient(to bottom, #f93131 1%,#474545 100%,#060606 100%); margin-left: 100px; border-radius: 15px; text-align: center;" id="segundo_card">
                    <div class="card-body">
                      <h5 class="card-title"></h5>
                      <div class="card-body">
                        <h1 class="card-title">{{ number_format((float)100/$qtd_animais_suinos*$qtd_nao_vacinados, 2, '.', '') }} %</h1>
                        <h2 class="card-text" style="color: white;">Suínos Não Vacinados</h2>
                      </div>
                    </div>
                  </div>

              </div>
            </div>
        </div>

        <script type="text/javascript">

          $('#primeiro_card').click(function(){
            window.location.href="/SuinosVacinados";
          })

          $('#segundo_card').click(function(){
            window.location.href="/SuinosNaoVacinados";
          })

        </script>

    </body>
</html>
