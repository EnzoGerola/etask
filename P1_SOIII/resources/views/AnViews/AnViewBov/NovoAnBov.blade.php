<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- BTS MD -->
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

        <!-- JQUERY IMPORT -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #181616;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                max-width: 100%;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                display: grid;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

              <div class="container">
                      <div class="card">
                        <div class="card-body">
                          <h5 class="card-title">Novo Registro</h5>

                          @if($errors->any())
                            <div class="alert alert-danger">
                              <ul class="list-group">
                                @foreach($errors->all() as $erro)

                                  <li class="list-group">
                                    {{ $erro }}
                                  </li>

                                @endforeach
                              </ul>
                            </div>
                          @endif

                          <form action="/RegistraAni" method="post">
                            @csrf
                            <div class="form-group">
                              <label for="a_tipo">Tipo do Animal</label>
                              <select id="a_tipo" name="a_tipo" class="form-control">
                                <option selected value="bovino">Bovino</option>
                              </select>
                            </div>

                            <div class="form-group">
                              <label for="a_raca">Digite a Raça do Animal</label>
                              <input type="text" class="form-control" id="a_raca" name="a_raca" placeholder="Raça do Animal">
                            </div>

                            <div class="form-group">
                              <label for="a_peso">Peso do Animal</label>
                              <input type="text" class="form-control" id="a_peso" name="a_peso" placeholder="Peso">
                            </div>
                            <div class="form-group">
                              <label for="a_sexo">Gênero</label>
                              <select id="a_sexo" name="a_sexo" class="form-control">
                                <option selected>Selecione</option>
                                <option value="masc">Macho</option>
                                <option value="femin">Fêmea</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="a_vacin">Vacinado</label>
                              <select id="a_vacin" name="a_vacin" class="form-control">
                                <option selected>Selecione</option>
                                <option value="1">Sim</option>
                                <option value="0">Não</option>
                              </select>
                            </div>

                            <input type="text" class="form-control" id="a_vacinas" name="a_vacinas" placeholder="Vacinas">

                            <div class="form-group">
                              <label for="a_data_nasc">Data de Nascimento do Animal</label>
                              <input type="date" class="form-control" id="a_data_nasc" name="a_data_nasc" placeholder="Data de Nascimento">
                            </div>
                            <br />
                              <button type="submit" class="btn btn-outline-success btn-block">Registrar!</button>
                          </form>
                        </div>
                      </div>
              </div>

        </div>

        <script type="text/javascript">

          $('#a_vacinas').hide();

          $('#a_vacin').change(function(){
            if($('#a_vacin').val() == '1'){
              $('#a_vacinas').show();
            }else{
              $('#a_vacinas').hide();
            }
          });

        </script>
    </body>
</html>
