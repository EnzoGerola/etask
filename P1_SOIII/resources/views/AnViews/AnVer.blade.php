<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- BTS MD -->
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #181616;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                max-width: 100%;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: left;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                display: grid;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
              <div class="row justify-content-center">

                <ul class="list-group">
                  <li class="list-group-item">
                    <div class="card" style="width: 25rem;">
                      <div class="card-body">
                        <h5 class="card-title">Tipo: {{ $Animal->a_tipo }}</h5>
                        <p class="card-text">Peso: {{ $Animal->a_peso }}</p>
                        @if($Animal->a_sexo == 'masc')
                          <p class="card-text">Macho</p>
                        @else
                          <p class="card-text">Fêmea</p>
                        @endif
                        @if($Animal->a_vacin == '1')
                          <p class="card-text">Vacinado</p>
                        @else
                          <p class="card-text">Não Vacinado</p>
                        @endif
                        @if($Animal->a_tipo == 'bovino' && $Animal->a_data_nasc != null || $Animal->a_tipo == 'bovino' && $Animal->a_raca != null)
                          <p class="card-text">Data de Nascimento: {{ $Animal->a_data_nasc }}</p>
                          <p class="card-text">Raça: {{ $Animal->a_raca }}</p>
                        @else
                          <p class="card-text" style="display: none;">Data de Nascimento: {{ $Animal->a_data_nasc }}</p>
                          <p class="card-text" style="display: none;">Raça: {{ $Animal->a_raca }}</p>
                        @endif
                        @if($Animal->a_peso != '0' && $Animal->a_peso != null)
                          <h3>R$ {{ number_format((float)$Animal->a_peso / 30 * 155, 2, ',', '') }}</h3>
                        @endif
                        <a href="/EditarAni/{{ $Animal->id_animal }}/EditarAni" class="card-btn"><button type="button" class="btn btn-outline-primary btn-block">Editar Informações</button></a>
                        <a href="/DeletaAnimal/{{ $Animal->id_animal }}/DeletaAnimal" class="card-btn"><button type="button" class="btn btn-outline-danger btn-block">Remover Registro</button></a>
                      </div>
                    </div>
                  </li>
                </ul>

              </div>
            </div>
        </div>
    </body>
</html>
