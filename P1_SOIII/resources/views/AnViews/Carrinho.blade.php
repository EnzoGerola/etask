<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- BTS MD -->
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

        <!-- JQUERY IMPORT -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

        <!-- Styles -->
        <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100);

body {
  background-color: #181616;
  color: #636b6f;
  font-family: 'Nunito', sans-serif;
  font-weight: 200;
  max-width: 100%;
}

div.table-title {
display: block;
margin: auto;
max-width: 600px;
padding:5px;
width: 100%;
}

.table-title h3 {
color: #fafafa;
font-size: 30px;
font-weight: 400;
font-style:normal;
font-family: "Roboto", helvetica, arial, sans-serif;
text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
text-transform:uppercase;
}


/*** Table Styles **/

.table-fill {
background: white;
border-radius:3px;
border-collapse: collapse;
height: 320px;
margin: auto;
max-width: 600px;
padding:5px;
width: 100%;
box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
animation: float 5s infinite;
}

th {
color:#D5DDE5;;
background:#1b1e24;
border-bottom:4px solid #9ea7af;
border-right: 1px solid #343a45;
font-size:23px;
font-weight: 100;
padding:24px;
text-align:left;
text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
vertical-align:middle;
}

th:first-child {
border-top-left-radius:3px;
}

th:last-child {
border-top-right-radius:3px;
border-right:none;
}

tr {
border-top: 1px solid #C1C3D1;
border-bottom-: 1px solid #C1C3D1;
color:#666B85;
font-size:16px;
font-weight:normal;
text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
}

tr:hover td {
background:#4E5066;
color:#FFFFFF;
border-top: 1px solid #22262e;
}

tr:first-child {
border-top:none;
}

tr:last-child {
border-bottom:none;
}

tr:nth-child(odd) td {
background:#EBEBEB;
}

tr:nth-child(odd):hover td {
background:#4E5066;
}

tr:last-child td:first-child {
border-bottom-left-radius:3px;
}

tr:last-child td:last-child {
border-bottom-right-radius:3px;
}

td {
background:#FFFFFF;
padding:20px;
text-align:left;
vertical-align:middle;
font-weight:300;
font-size:18px;
text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
border-right: 1px solid #C1C3D1;
}

td:last-child {
border-right: 0px;
}

th.text-left {
text-align: left;
}

th.text-center {
text-align: center;
}

th.text-right {
text-align: right;
}

td.text-left {
text-align: left;
}

td.text-center {
text-align: center;
}

td.text-right {
text-align: right;
}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="row justify-content-center">
                  <table>
                    <thead>
                      <tr>
                        <th>Peso Do Animal</th>
                        <th>Preço Do Animal</th>
                        <th>Raça Do Animal</th>
                        <th>Gênero Do Animal</th>
                        <th>Tipo Do Animal</th>
                        <th>Data/Hora da compra Do Animal</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($Compra as $tds_comp)
                      <tr style="text-align: center;">
                        <td>
                          <p>{{ $tds_comp->a_peso }} KG</p>
                        </td>
                        <td>
                          <p>R$ {{ $tds_comp->a_preco }}</p>
                        </td>
                        <td>
                          <p>{{ $tds_comp->a_raca }}</p>
                        </td>
                        @if($tds_comp->a_gen == 'masc')
                        <td>
                          <p>Macho</p>
                        </td>
                        @elseif($tds_comp->a_gen == 'femin')
                        <td>
                          <p>Fêmea</p>
                        </td>
                        @endif
                        <td>
                          <p>{{ $tds_comp->a_tipo }}</p>
                        </td>
                        <td>
                          <p>{{ $tds_comp->created_at }}</p>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
        </div>

    </body>
</html>
