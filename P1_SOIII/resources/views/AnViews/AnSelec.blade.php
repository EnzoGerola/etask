<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- BTS MD -->
        <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #181616;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                max-width: 100%;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: left;
            }

            .title {
                font-size: 84px;
            }

            .card:hover{
              opacity: 0.9;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
              <div class="row justify-content-center">

                <ul class="list-group">
                  <li class="list-group-item">
                    <div class="card" style="width: 18rem; height: 25rem; border-radius: 20px;">
                      <img class="card-img-top" src="https://cptstatic.s3.amazonaws.com/imagens/enviadas/materias/materia8683/gado-de-corte-cursos-cpt.jpg" alt="Card image cap">
                      <div class="card-body">
                        <h5 class="card-title">Cadastro de animal bovino</h5>
                        <p class="card-text"></p>
                        <a href="/NovoAnBov" class="btn btn-outline-dark" style="margin-bottom: -120px;">Cadastrar</a>
                      </div>
                    </div>
                    <div class="card" style="width: 18rem; height: 25rem; border-radius: 20px;">
                      <img class="card-img-top" src="https://cptstatic.s3.amazonaws.com/imagens/produtos/500px/45458/producao-de-suino-light-mais-carne-menos-gordura-01.jpg" alt="Card image cap">
                      <div class="card-body">
                        <h5 class="card-title">Cadastro de animal suíno</h5>
                        <p class="card-text"></p>
                        <a href="/NovoAnSuin" class="btn btn-outline-dark" style="margin-bottom: -95px;">Cadastrar</a>
                      </div>
                    </div>
                  </li>
                </ul>

              </div>
            </div>
        </div>
    </body>
</html>
