<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Rebanho extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('animais', function (Blueprint $table) {
          $table->bigIncrements('id_animal');
          $table->string('a_tipo');
          $table->string('a_peso');
          $table->string('a_qtd');
          $table->integer('a_preco');
          $table->string('a_sexo');
          $table->boolean('a_vacin');
          $table->rememberToken();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
