<?php
Route::get('/', function () {
    return view('welcome');
});

//VIEW ANIMAIS
Route::get('IndexAni', 'AnimaisController@IndexAni');
Route::get('AnVer/{animal}', 'AnimaisController@VerAnimais');
Route::get('AnEstats', 'AnimaisController@AnEstats');
Route::get('AniVacinados', 'AniSuinController@SuinosVacinados');
Route::get('Carrinho', 'AnimaisController@VerCarrinho');
Route::get('NovoMenu', 'AnimaisController@NovoMenu');

//VER ANIMAIS SUINOS
Route::get('EstatisticasSuinos', 'AniSuinController@EstatsSuinos');
Route::get('SuinosVacinados', 'AniSuinController@SuinosVacinados');
Route::get('SuinosNaoVacinados', 'AniSuinController@SuinosNaoVacinados');

//VER ANIMAIS BOVINOS
Route::get('EstatisticasBovinos', 'AniBovController@EstatsBovinos');
Route::get('BovinosVacinados', 'AniBovController@BovinosVacinados');
Route::get('BovinosNaoVacinados', 'AniBovController@BovinosNaoVacinados');

//INSERIR NOVO ANIMAL
Route::get('NovoA', 'AnimaisController@NovoAni');
Route::get('NovoAnBov', 'AniBovController@NovoAnBov');
Route::get('NovoAnSuin', 'AniSuinController@NovoAnSuin');
Route::post('RegistraAni', 'AnimaisController@RegistraAni');

//EDITAR ANIMAL
Route::get('EditarAni/{animal}/EditarAni', 'AnimaisController@EditarAni');
Route::post('EditarAni/{animal}/AttAnimal', 'AnimaisController@AttAnimal');

//COMPRAR ANIMAL
Route::get('ComprarAnimal/{animal}/ComprarAnimal', 'AnimaisController@ComprarAnimal');

//DELETAR ETASKS
Route::get('DeletaAnimal/{animal}/DeletaAnimal', 'AnimaisController@DeletaAnimal');
