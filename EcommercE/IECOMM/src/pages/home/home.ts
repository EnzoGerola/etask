import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as WC from 'woocommerce-api';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  WooCommerce: any;

  constructor(public navCtrl: NavController) {

    this.WooCommerce = WC({

      url: 'http://localhost/PROJETOS/EcommercE/',
      consumerKey: 'ck_43857933ca44a5e63744ea01b9ea0a17b9a4c527',
      consumerSecret: 'cs_0cea61a222f4b7d14c9ddda817a10450e53e6bbf',
      wpAPI: true,
      version: 'wc/v3',
      queryStringAuth: true
    });

    this.WooCommerce.getAsync('products').then((data) => {
      console.log(data);
    }, (err) => {
      console.log(err);
    });

  }

}
